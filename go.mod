module uhbot

go 1.17

require (
	entgo.io/ent v0.8.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/mb-14/gomarkov v0.0.0-20210216094942-a5b484cc0243
	github.com/robfig/cron/v3 v3.0.0
	github.com/sirupsen/logrus v1.2.0
	gopkg.in/tucnak/telebot.v2 v2.3.5
)

require (
	github.com/google/uuid v1.2.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/sys v0.0.0-20210320140829-1e4c9ba3b0c4 // indirect
)
