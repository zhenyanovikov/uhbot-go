package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
)
import "entgo.io/ent/schema/field"

// Student holds the schema definition for the Student entity.
type Student struct {
	ent.Schema
}

// Fields of the Student.
func (Student) Fields() []ent.Field {
	return []ent.Field{
		field.Int("telegram_id").Unique(),
		field.String("name").NotEmpty(),
		field.Bool("is_admin").Default(false),
	}
}

// Edges of the Student.
func (Student) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("answers", PollStudentAnswer.Type),
	}
}
