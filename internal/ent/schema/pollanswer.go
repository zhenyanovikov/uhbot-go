package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// PollAnswer holds the schema definition for the PollAnswer entity.
type PollAnswer struct {
	ent.Schema
}

// Fields of the PollAnswer.
func (PollAnswer) Fields() []ent.Field {
	return []ent.Field{
		field.String("text").NotEmpty(),
		field.String("response").NotEmpty(),
	}
}

// Edges of the PollAnswer.
func (PollAnswer) Edges() []ent.Edge {
	return []ent.Edge{
		// To, From
		edge.To("studentAnswers", PollStudentAnswer.Type),
		edge.From("poll", Poll.Type).
			Ref("possibleAnswers"),
	}
}
