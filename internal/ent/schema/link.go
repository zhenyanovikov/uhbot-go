package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// Link holds the schema definition for the Link entity.
type Link struct {
	ent.Schema
}

// Fields of the Link.
func (Link) Fields() []ent.Field {
	return []ent.Field{
		field.String("teacher").Unique(),
		field.String("subject"),
		field.String("subject_type"),
		field.String("platform").Default("???"),
		field.String("url").Optional().Nillable(),
	}
}

// Edges of the Link.
func (Link) Edges() []ent.Edge {
	return nil
}
