// Code generated by entc, DO NOT EDIT.

package student

const (
	// Label holds the string label denoting the student type in the database.
	Label = "student"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldTelegramID holds the string denoting the telegram_id field in the database.
	FieldTelegramID = "telegram_id"
	// FieldName holds the string denoting the name field in the database.
	FieldName = "name"
	// FieldIsAdmin holds the string denoting the is_admin field in the database.
	FieldIsAdmin = "is_admin"
	// EdgeAnswers holds the string denoting the answers edge name in mutations.
	EdgeAnswers = "answers"
	// Table holds the table name of the student in the database.
	Table = "students"
	// AnswersTable is the table the holds the answers relation/edge.
	AnswersTable = "poll_student_answers"
	// AnswersInverseTable is the table name for the PollStudentAnswer entity.
	// It exists in this package in order to avoid circular dependency with the "pollstudentanswer" package.
	AnswersInverseTable = "poll_student_answers"
	// AnswersColumn is the table column denoting the answers relation/edge.
	AnswersColumn = "student_answers"
)

// Columns holds all SQL columns for student fields.
var Columns = []string{
	FieldID,
	FieldTelegramID,
	FieldName,
	FieldIsAdmin,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}

var (
	// NameValidator is a validator for the "name" field. It is called by the builders before save.
	NameValidator func(string) error
	// DefaultIsAdmin holds the default value on creation for the "is_admin" field.
	DefaultIsAdmin bool
)
