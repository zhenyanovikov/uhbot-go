// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"
	"uhbot/internal/ent/poll"
	"uhbot/internal/ent/pollanswer"
	"uhbot/internal/ent/pollstudentanswer"
	"uhbot/internal/ent/predicate"
	"uhbot/internal/ent/student"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// PollStudentAnswerUpdate is the builder for updating PollStudentAnswer entities.
type PollStudentAnswerUpdate struct {
	config
	hooks    []Hook
	mutation *PollStudentAnswerMutation
}

// Where adds a new predicate for the PollStudentAnswerUpdate builder.
func (psau *PollStudentAnswerUpdate) Where(ps ...predicate.PollStudentAnswer) *PollStudentAnswerUpdate {
	psau.mutation.predicates = append(psau.mutation.predicates, ps...)
	return psau
}

// SetPollID sets the "poll" edge to the Poll entity by ID.
func (psau *PollStudentAnswerUpdate) SetPollID(id int) *PollStudentAnswerUpdate {
	psau.mutation.SetPollID(id)
	return psau
}

// SetNillablePollID sets the "poll" edge to the Poll entity by ID if the given value is not nil.
func (psau *PollStudentAnswerUpdate) SetNillablePollID(id *int) *PollStudentAnswerUpdate {
	if id != nil {
		psau = psau.SetPollID(*id)
	}
	return psau
}

// SetPoll sets the "poll" edge to the Poll entity.
func (psau *PollStudentAnswerUpdate) SetPoll(p *Poll) *PollStudentAnswerUpdate {
	return psau.SetPollID(p.ID)
}

// SetStudentID sets the "student" edge to the Student entity by ID.
func (psau *PollStudentAnswerUpdate) SetStudentID(id int) *PollStudentAnswerUpdate {
	psau.mutation.SetStudentID(id)
	return psau
}

// SetNillableStudentID sets the "student" edge to the Student entity by ID if the given value is not nil.
func (psau *PollStudentAnswerUpdate) SetNillableStudentID(id *int) *PollStudentAnswerUpdate {
	if id != nil {
		psau = psau.SetStudentID(*id)
	}
	return psau
}

// SetStudent sets the "student" edge to the Student entity.
func (psau *PollStudentAnswerUpdate) SetStudent(s *Student) *PollStudentAnswerUpdate {
	return psau.SetStudentID(s.ID)
}

// SetAnswerID sets the "answer" edge to the PollAnswer entity by ID.
func (psau *PollStudentAnswerUpdate) SetAnswerID(id int) *PollStudentAnswerUpdate {
	psau.mutation.SetAnswerID(id)
	return psau
}

// SetNillableAnswerID sets the "answer" edge to the PollAnswer entity by ID if the given value is not nil.
func (psau *PollStudentAnswerUpdate) SetNillableAnswerID(id *int) *PollStudentAnswerUpdate {
	if id != nil {
		psau = psau.SetAnswerID(*id)
	}
	return psau
}

// SetAnswer sets the "answer" edge to the PollAnswer entity.
func (psau *PollStudentAnswerUpdate) SetAnswer(p *PollAnswer) *PollStudentAnswerUpdate {
	return psau.SetAnswerID(p.ID)
}

// Mutation returns the PollStudentAnswerMutation object of the builder.
func (psau *PollStudentAnswerUpdate) Mutation() *PollStudentAnswerMutation {
	return psau.mutation
}

// ClearPoll clears the "poll" edge to the Poll entity.
func (psau *PollStudentAnswerUpdate) ClearPoll() *PollStudentAnswerUpdate {
	psau.mutation.ClearPoll()
	return psau
}

// ClearStudent clears the "student" edge to the Student entity.
func (psau *PollStudentAnswerUpdate) ClearStudent() *PollStudentAnswerUpdate {
	psau.mutation.ClearStudent()
	return psau
}

// ClearAnswer clears the "answer" edge to the PollAnswer entity.
func (psau *PollStudentAnswerUpdate) ClearAnswer() *PollStudentAnswerUpdate {
	psau.mutation.ClearAnswer()
	return psau
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (psau *PollStudentAnswerUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(psau.hooks) == 0 {
		affected, err = psau.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*PollStudentAnswerMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			psau.mutation = mutation
			affected, err = psau.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(psau.hooks) - 1; i >= 0; i-- {
			mut = psau.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, psau.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (psau *PollStudentAnswerUpdate) SaveX(ctx context.Context) int {
	affected, err := psau.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (psau *PollStudentAnswerUpdate) Exec(ctx context.Context) error {
	_, err := psau.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (psau *PollStudentAnswerUpdate) ExecX(ctx context.Context) {
	if err := psau.Exec(ctx); err != nil {
		panic(err)
	}
}

func (psau *PollStudentAnswerUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   pollstudentanswer.Table,
			Columns: pollstudentanswer.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: pollstudentanswer.FieldID,
			},
		},
	}
	if ps := psau.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if psau.mutation.PollCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.PollTable,
			Columns: []string{pollstudentanswer.PollColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: poll.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := psau.mutation.PollIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.PollTable,
			Columns: []string{pollstudentanswer.PollColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: poll.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if psau.mutation.StudentCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.StudentTable,
			Columns: []string{pollstudentanswer.StudentColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: student.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := psau.mutation.StudentIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.StudentTable,
			Columns: []string{pollstudentanswer.StudentColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: student.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if psau.mutation.AnswerCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.AnswerTable,
			Columns: []string{pollstudentanswer.AnswerColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := psau.mutation.AnswerIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.AnswerTable,
			Columns: []string{pollstudentanswer.AnswerColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, psau.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{pollstudentanswer.Label}
		} else if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return 0, err
	}
	return n, nil
}

// PollStudentAnswerUpdateOne is the builder for updating a single PollStudentAnswer entity.
type PollStudentAnswerUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *PollStudentAnswerMutation
}

// SetPollID sets the "poll" edge to the Poll entity by ID.
func (psauo *PollStudentAnswerUpdateOne) SetPollID(id int) *PollStudentAnswerUpdateOne {
	psauo.mutation.SetPollID(id)
	return psauo
}

// SetNillablePollID sets the "poll" edge to the Poll entity by ID if the given value is not nil.
func (psauo *PollStudentAnswerUpdateOne) SetNillablePollID(id *int) *PollStudentAnswerUpdateOne {
	if id != nil {
		psauo = psauo.SetPollID(*id)
	}
	return psauo
}

// SetPoll sets the "poll" edge to the Poll entity.
func (psauo *PollStudentAnswerUpdateOne) SetPoll(p *Poll) *PollStudentAnswerUpdateOne {
	return psauo.SetPollID(p.ID)
}

// SetStudentID sets the "student" edge to the Student entity by ID.
func (psauo *PollStudentAnswerUpdateOne) SetStudentID(id int) *PollStudentAnswerUpdateOne {
	psauo.mutation.SetStudentID(id)
	return psauo
}

// SetNillableStudentID sets the "student" edge to the Student entity by ID if the given value is not nil.
func (psauo *PollStudentAnswerUpdateOne) SetNillableStudentID(id *int) *PollStudentAnswerUpdateOne {
	if id != nil {
		psauo = psauo.SetStudentID(*id)
	}
	return psauo
}

// SetStudent sets the "student" edge to the Student entity.
func (psauo *PollStudentAnswerUpdateOne) SetStudent(s *Student) *PollStudentAnswerUpdateOne {
	return psauo.SetStudentID(s.ID)
}

// SetAnswerID sets the "answer" edge to the PollAnswer entity by ID.
func (psauo *PollStudentAnswerUpdateOne) SetAnswerID(id int) *PollStudentAnswerUpdateOne {
	psauo.mutation.SetAnswerID(id)
	return psauo
}

// SetNillableAnswerID sets the "answer" edge to the PollAnswer entity by ID if the given value is not nil.
func (psauo *PollStudentAnswerUpdateOne) SetNillableAnswerID(id *int) *PollStudentAnswerUpdateOne {
	if id != nil {
		psauo = psauo.SetAnswerID(*id)
	}
	return psauo
}

// SetAnswer sets the "answer" edge to the PollAnswer entity.
func (psauo *PollStudentAnswerUpdateOne) SetAnswer(p *PollAnswer) *PollStudentAnswerUpdateOne {
	return psauo.SetAnswerID(p.ID)
}

// Mutation returns the PollStudentAnswerMutation object of the builder.
func (psauo *PollStudentAnswerUpdateOne) Mutation() *PollStudentAnswerMutation {
	return psauo.mutation
}

// ClearPoll clears the "poll" edge to the Poll entity.
func (psauo *PollStudentAnswerUpdateOne) ClearPoll() *PollStudentAnswerUpdateOne {
	psauo.mutation.ClearPoll()
	return psauo
}

// ClearStudent clears the "student" edge to the Student entity.
func (psauo *PollStudentAnswerUpdateOne) ClearStudent() *PollStudentAnswerUpdateOne {
	psauo.mutation.ClearStudent()
	return psauo
}

// ClearAnswer clears the "answer" edge to the PollAnswer entity.
func (psauo *PollStudentAnswerUpdateOne) ClearAnswer() *PollStudentAnswerUpdateOne {
	psauo.mutation.ClearAnswer()
	return psauo
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (psauo *PollStudentAnswerUpdateOne) Select(field string, fields ...string) *PollStudentAnswerUpdateOne {
	psauo.fields = append([]string{field}, fields...)
	return psauo
}

// Save executes the query and returns the updated PollStudentAnswer entity.
func (psauo *PollStudentAnswerUpdateOne) Save(ctx context.Context) (*PollStudentAnswer, error) {
	var (
		err  error
		node *PollStudentAnswer
	)
	if len(psauo.hooks) == 0 {
		node, err = psauo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*PollStudentAnswerMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			psauo.mutation = mutation
			node, err = psauo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(psauo.hooks) - 1; i >= 0; i-- {
			mut = psauo.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, psauo.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (psauo *PollStudentAnswerUpdateOne) SaveX(ctx context.Context) *PollStudentAnswer {
	node, err := psauo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (psauo *PollStudentAnswerUpdateOne) Exec(ctx context.Context) error {
	_, err := psauo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (psauo *PollStudentAnswerUpdateOne) ExecX(ctx context.Context) {
	if err := psauo.Exec(ctx); err != nil {
		panic(err)
	}
}

func (psauo *PollStudentAnswerUpdateOne) sqlSave(ctx context.Context) (_node *PollStudentAnswer, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   pollstudentanswer.Table,
			Columns: pollstudentanswer.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: pollstudentanswer.FieldID,
			},
		},
	}
	id, ok := psauo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "ID", err: fmt.Errorf("missing PollStudentAnswer.ID for update")}
	}
	_spec.Node.ID.Value = id
	if fields := psauo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, pollstudentanswer.FieldID)
		for _, f := range fields {
			if !pollstudentanswer.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != pollstudentanswer.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := psauo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if psauo.mutation.PollCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.PollTable,
			Columns: []string{pollstudentanswer.PollColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: poll.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := psauo.mutation.PollIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.PollTable,
			Columns: []string{pollstudentanswer.PollColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: poll.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if psauo.mutation.StudentCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.StudentTable,
			Columns: []string{pollstudentanswer.StudentColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: student.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := psauo.mutation.StudentIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.StudentTable,
			Columns: []string{pollstudentanswer.StudentColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: student.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if psauo.mutation.AnswerCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.AnswerTable,
			Columns: []string{pollstudentanswer.AnswerColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := psauo.mutation.AnswerIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   pollstudentanswer.AnswerTable,
			Columns: []string{pollstudentanswer.AnswerColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	_node = &PollStudentAnswer{config: psauo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, psauo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{pollstudentanswer.Label}
		} else if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	return _node, nil
}
