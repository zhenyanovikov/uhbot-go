// Code generated by entc, DO NOT EDIT.

package queueentity

const (
	// Label holds the string label denoting the queueentity type in the database.
	Label = "queue_entity"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldName holds the string denoting the name field in the database.
	FieldName = "name"
	// FieldPosition holds the string denoting the position field in the database.
	FieldPosition = "position"
	// EdgeQueue holds the string denoting the queue edge name in mutations.
	EdgeQueue = "queue"
	// Table holds the table name of the queueentity in the database.
	Table = "queue_entities"
	// QueueTable is the table the holds the queue relation/edge. The primary key declared below.
	QueueTable = "queue_all_entities"
	// QueueInverseTable is the table name for the Queue entity.
	// It exists in this package in order to avoid circular dependency with the "queue" package.
	QueueInverseTable = "queues"
)

// Columns holds all SQL columns for queueentity fields.
var Columns = []string{
	FieldID,
	FieldName,
	FieldPosition,
}

var (
	// QueuePrimaryKey and QueueColumn2 are the table columns denoting the
	// primary key for the queue relation (M2M).
	QueuePrimaryKey = []string{"queue_id", "queue_entity_id"}
)

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}

var (
	// NameValidator is a validator for the "name" field. It is called by the builders before save.
	NameValidator func(string) error
)
