// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"uhbot/internal/ent/poll"
	"uhbot/internal/ent/pollanswer"
	"uhbot/internal/ent/pollstudentanswer"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// PollAnswerCreate is the builder for creating a PollAnswer entity.
type PollAnswerCreate struct {
	config
	mutation *PollAnswerMutation
	hooks    []Hook
}

// SetText sets the "text" field.
func (pac *PollAnswerCreate) SetText(s string) *PollAnswerCreate {
	pac.mutation.SetText(s)
	return pac
}

// SetResponse sets the "response" field.
func (pac *PollAnswerCreate) SetResponse(s string) *PollAnswerCreate {
	pac.mutation.SetResponse(s)
	return pac
}

// AddStudentAnswerIDs adds the "studentAnswers" edge to the PollStudentAnswer entity by IDs.
func (pac *PollAnswerCreate) AddStudentAnswerIDs(ids ...int) *PollAnswerCreate {
	pac.mutation.AddStudentAnswerIDs(ids...)
	return pac
}

// AddStudentAnswers adds the "studentAnswers" edges to the PollStudentAnswer entity.
func (pac *PollAnswerCreate) AddStudentAnswers(p ...*PollStudentAnswer) *PollAnswerCreate {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return pac.AddStudentAnswerIDs(ids...)
}

// AddPollIDs adds the "poll" edge to the Poll entity by IDs.
func (pac *PollAnswerCreate) AddPollIDs(ids ...int) *PollAnswerCreate {
	pac.mutation.AddPollIDs(ids...)
	return pac
}

// AddPoll adds the "poll" edges to the Poll entity.
func (pac *PollAnswerCreate) AddPoll(p ...*Poll) *PollAnswerCreate {
	ids := make([]int, len(p))
	for i := range p {
		ids[i] = p[i].ID
	}
	return pac.AddPollIDs(ids...)
}

// Mutation returns the PollAnswerMutation object of the builder.
func (pac *PollAnswerCreate) Mutation() *PollAnswerMutation {
	return pac.mutation
}

// Save creates the PollAnswer in the database.
func (pac *PollAnswerCreate) Save(ctx context.Context) (*PollAnswer, error) {
	var (
		err  error
		node *PollAnswer
	)
	if len(pac.hooks) == 0 {
		if err = pac.check(); err != nil {
			return nil, err
		}
		node, err = pac.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*PollAnswerMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = pac.check(); err != nil {
				return nil, err
			}
			pac.mutation = mutation
			node, err = pac.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(pac.hooks) - 1; i >= 0; i-- {
			mut = pac.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, pac.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (pac *PollAnswerCreate) SaveX(ctx context.Context) *PollAnswer {
	v, err := pac.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// check runs all checks and user-defined validators on the builder.
func (pac *PollAnswerCreate) check() error {
	if _, ok := pac.mutation.Text(); !ok {
		return &ValidationError{Name: "text", err: errors.New("ent: missing required field \"text\"")}
	}
	if v, ok := pac.mutation.Text(); ok {
		if err := pollanswer.TextValidator(v); err != nil {
			return &ValidationError{Name: "text", err: fmt.Errorf("ent: validator failed for field \"text\": %w", err)}
		}
	}
	if _, ok := pac.mutation.Response(); !ok {
		return &ValidationError{Name: "response", err: errors.New("ent: missing required field \"response\"")}
	}
	if v, ok := pac.mutation.Response(); ok {
		if err := pollanswer.ResponseValidator(v); err != nil {
			return &ValidationError{Name: "response", err: fmt.Errorf("ent: validator failed for field \"response\": %w", err)}
		}
	}
	return nil
}

func (pac *PollAnswerCreate) sqlSave(ctx context.Context) (*PollAnswer, error) {
	_node, _spec := pac.createSpec()
	if err := sqlgraph.CreateNode(ctx, pac.driver, _spec); err != nil {
		if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	_node.ID = int(id)
	return _node, nil
}

func (pac *PollAnswerCreate) createSpec() (*PollAnswer, *sqlgraph.CreateSpec) {
	var (
		_node = &PollAnswer{config: pac.config}
		_spec = &sqlgraph.CreateSpec{
			Table: pollanswer.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: pollanswer.FieldID,
			},
		}
	)
	if value, ok := pac.mutation.Text(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: pollanswer.FieldText,
		})
		_node.Text = value
	}
	if value, ok := pac.mutation.Response(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: pollanswer.FieldResponse,
		})
		_node.Response = value
	}
	if nodes := pac.mutation.StudentAnswersIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2M,
			Inverse: false,
			Table:   pollanswer.StudentAnswersTable,
			Columns: []string{pollanswer.StudentAnswersColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: pollstudentanswer.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	if nodes := pac.mutation.PollIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   pollanswer.PollTable,
			Columns: pollanswer.PollPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: poll.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// PollAnswerCreateBulk is the builder for creating many PollAnswer entities in bulk.
type PollAnswerCreateBulk struct {
	config
	builders []*PollAnswerCreate
}

// Save creates the PollAnswer entities in the database.
func (pacb *PollAnswerCreateBulk) Save(ctx context.Context) ([]*PollAnswer, error) {
	specs := make([]*sqlgraph.CreateSpec, len(pacb.builders))
	nodes := make([]*PollAnswer, len(pacb.builders))
	mutators := make([]Mutator, len(pacb.builders))
	for i := range pacb.builders {
		func(i int, root context.Context) {
			builder := pacb.builders[i]
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*PollAnswerMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, pacb.builders[i+1].mutation)
				} else {
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, pacb.driver, &sqlgraph.BatchCreateSpec{Nodes: specs}); err != nil {
						if cerr, ok := isSQLConstraintError(err); ok {
							err = cerr
						}
					}
				}
				mutation.done = true
				if err != nil {
					return nil, err
				}
				id := specs[i].ID.Value.(int64)
				nodes[i].ID = int(id)
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, pacb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (pacb *PollAnswerCreateBulk) SaveX(ctx context.Context) []*PollAnswer {
	v, err := pacb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}
