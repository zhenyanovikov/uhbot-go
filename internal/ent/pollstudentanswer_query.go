// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"math"
	"uhbot/internal/ent/poll"
	"uhbot/internal/ent/pollanswer"
	"uhbot/internal/ent/pollstudentanswer"
	"uhbot/internal/ent/predicate"
	"uhbot/internal/ent/student"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// PollStudentAnswerQuery is the builder for querying PollStudentAnswer entities.
type PollStudentAnswerQuery struct {
	config
	limit      *int
	offset     *int
	unique     *bool
	order      []OrderFunc
	fields     []string
	predicates []predicate.PollStudentAnswer
	// eager-loading edges.
	withPoll    *PollQuery
	withStudent *StudentQuery
	withAnswer  *PollAnswerQuery
	withFKs     bool
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the PollStudentAnswerQuery builder.
func (psaq *PollStudentAnswerQuery) Where(ps ...predicate.PollStudentAnswer) *PollStudentAnswerQuery {
	psaq.predicates = append(psaq.predicates, ps...)
	return psaq
}

// Limit adds a limit step to the query.
func (psaq *PollStudentAnswerQuery) Limit(limit int) *PollStudentAnswerQuery {
	psaq.limit = &limit
	return psaq
}

// Offset adds an offset step to the query.
func (psaq *PollStudentAnswerQuery) Offset(offset int) *PollStudentAnswerQuery {
	psaq.offset = &offset
	return psaq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (psaq *PollStudentAnswerQuery) Unique(unique bool) *PollStudentAnswerQuery {
	psaq.unique = &unique
	return psaq
}

// Order adds an order step to the query.
func (psaq *PollStudentAnswerQuery) Order(o ...OrderFunc) *PollStudentAnswerQuery {
	psaq.order = append(psaq.order, o...)
	return psaq
}

// QueryPoll chains the current query on the "poll" edge.
func (psaq *PollStudentAnswerQuery) QueryPoll() *PollQuery {
	query := &PollQuery{config: psaq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := psaq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := psaq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(pollstudentanswer.Table, pollstudentanswer.FieldID, selector),
			sqlgraph.To(poll.Table, poll.FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, pollstudentanswer.PollTable, pollstudentanswer.PollColumn),
		)
		fromU = sqlgraph.SetNeighbors(psaq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// QueryStudent chains the current query on the "student" edge.
func (psaq *PollStudentAnswerQuery) QueryStudent() *StudentQuery {
	query := &StudentQuery{config: psaq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := psaq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := psaq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(pollstudentanswer.Table, pollstudentanswer.FieldID, selector),
			sqlgraph.To(student.Table, student.FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, pollstudentanswer.StudentTable, pollstudentanswer.StudentColumn),
		)
		fromU = sqlgraph.SetNeighbors(psaq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// QueryAnswer chains the current query on the "answer" edge.
func (psaq *PollStudentAnswerQuery) QueryAnswer() *PollAnswerQuery {
	query := &PollAnswerQuery{config: psaq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := psaq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := psaq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(pollstudentanswer.Table, pollstudentanswer.FieldID, selector),
			sqlgraph.To(pollanswer.Table, pollanswer.FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, pollstudentanswer.AnswerTable, pollstudentanswer.AnswerColumn),
		)
		fromU = sqlgraph.SetNeighbors(psaq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// First returns the first PollStudentAnswer entity from the query.
// Returns a *NotFoundError when no PollStudentAnswer was found.
func (psaq *PollStudentAnswerQuery) First(ctx context.Context) (*PollStudentAnswer, error) {
	nodes, err := psaq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{pollstudentanswer.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) FirstX(ctx context.Context) *PollStudentAnswer {
	node, err := psaq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first PollStudentAnswer ID from the query.
// Returns a *NotFoundError when no PollStudentAnswer ID was found.
func (psaq *PollStudentAnswerQuery) FirstID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = psaq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{pollstudentanswer.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) FirstIDX(ctx context.Context) int {
	id, err := psaq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single PollStudentAnswer entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when exactly one PollStudentAnswer entity is not found.
// Returns a *NotFoundError when no PollStudentAnswer entities are found.
func (psaq *PollStudentAnswerQuery) Only(ctx context.Context) (*PollStudentAnswer, error) {
	nodes, err := psaq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{pollstudentanswer.Label}
	default:
		return nil, &NotSingularError{pollstudentanswer.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) OnlyX(ctx context.Context) *PollStudentAnswer {
	node, err := psaq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only PollStudentAnswer ID in the query.
// Returns a *NotSingularError when exactly one PollStudentAnswer ID is not found.
// Returns a *NotFoundError when no entities are found.
func (psaq *PollStudentAnswerQuery) OnlyID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = psaq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = &NotSingularError{pollstudentanswer.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) OnlyIDX(ctx context.Context) int {
	id, err := psaq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of PollStudentAnswers.
func (psaq *PollStudentAnswerQuery) All(ctx context.Context) ([]*PollStudentAnswer, error) {
	if err := psaq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return psaq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) AllX(ctx context.Context) []*PollStudentAnswer {
	nodes, err := psaq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of PollStudentAnswer IDs.
func (psaq *PollStudentAnswerQuery) IDs(ctx context.Context) ([]int, error) {
	var ids []int
	if err := psaq.Select(pollstudentanswer.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) IDsX(ctx context.Context) []int {
	ids, err := psaq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (psaq *PollStudentAnswerQuery) Count(ctx context.Context) (int, error) {
	if err := psaq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return psaq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) CountX(ctx context.Context) int {
	count, err := psaq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (psaq *PollStudentAnswerQuery) Exist(ctx context.Context) (bool, error) {
	if err := psaq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return psaq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (psaq *PollStudentAnswerQuery) ExistX(ctx context.Context) bool {
	exist, err := psaq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the PollStudentAnswerQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (psaq *PollStudentAnswerQuery) Clone() *PollStudentAnswerQuery {
	if psaq == nil {
		return nil
	}
	return &PollStudentAnswerQuery{
		config:      psaq.config,
		limit:       psaq.limit,
		offset:      psaq.offset,
		order:       append([]OrderFunc{}, psaq.order...),
		predicates:  append([]predicate.PollStudentAnswer{}, psaq.predicates...),
		withPoll:    psaq.withPoll.Clone(),
		withStudent: psaq.withStudent.Clone(),
		withAnswer:  psaq.withAnswer.Clone(),
		// clone intermediate query.
		sql:  psaq.sql.Clone(),
		path: psaq.path,
	}
}

// WithPoll tells the query-builder to eager-load the nodes that are connected to
// the "poll" edge. The optional arguments are used to configure the query builder of the edge.
func (psaq *PollStudentAnswerQuery) WithPoll(opts ...func(*PollQuery)) *PollStudentAnswerQuery {
	query := &PollQuery{config: psaq.config}
	for _, opt := range opts {
		opt(query)
	}
	psaq.withPoll = query
	return psaq
}

// WithStudent tells the query-builder to eager-load the nodes that are connected to
// the "student" edge. The optional arguments are used to configure the query builder of the edge.
func (psaq *PollStudentAnswerQuery) WithStudent(opts ...func(*StudentQuery)) *PollStudentAnswerQuery {
	query := &StudentQuery{config: psaq.config}
	for _, opt := range opts {
		opt(query)
	}
	psaq.withStudent = query
	return psaq
}

// WithAnswer tells the query-builder to eager-load the nodes that are connected to
// the "answer" edge. The optional arguments are used to configure the query builder of the edge.
func (psaq *PollStudentAnswerQuery) WithAnswer(opts ...func(*PollAnswerQuery)) *PollStudentAnswerQuery {
	query := &PollAnswerQuery{config: psaq.config}
	for _, opt := range opts {
		opt(query)
	}
	psaq.withAnswer = query
	return psaq
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
func (psaq *PollStudentAnswerQuery) GroupBy(field string, fields ...string) *PollStudentAnswerGroupBy {
	group := &PollStudentAnswerGroupBy{config: psaq.config}
	group.fields = append([]string{field}, fields...)
	group.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := psaq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return psaq.sqlQuery(ctx), nil
	}
	return group
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
func (psaq *PollStudentAnswerQuery) Select(field string, fields ...string) *PollStudentAnswerSelect {
	psaq.fields = append([]string{field}, fields...)
	return &PollStudentAnswerSelect{PollStudentAnswerQuery: psaq}
}

func (psaq *PollStudentAnswerQuery) prepareQuery(ctx context.Context) error {
	for _, f := range psaq.fields {
		if !pollstudentanswer.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if psaq.path != nil {
		prev, err := psaq.path(ctx)
		if err != nil {
			return err
		}
		psaq.sql = prev
	}
	return nil
}

func (psaq *PollStudentAnswerQuery) sqlAll(ctx context.Context) ([]*PollStudentAnswer, error) {
	var (
		nodes       = []*PollStudentAnswer{}
		withFKs     = psaq.withFKs
		_spec       = psaq.querySpec()
		loadedTypes = [3]bool{
			psaq.withPoll != nil,
			psaq.withStudent != nil,
			psaq.withAnswer != nil,
		}
	)
	if psaq.withPoll != nil || psaq.withStudent != nil || psaq.withAnswer != nil {
		withFKs = true
	}
	if withFKs {
		_spec.Node.Columns = append(_spec.Node.Columns, pollstudentanswer.ForeignKeys...)
	}
	_spec.ScanValues = func(columns []string) ([]interface{}, error) {
		node := &PollStudentAnswer{config: psaq.config}
		nodes = append(nodes, node)
		return node.scanValues(columns)
	}
	_spec.Assign = func(columns []string, values []interface{}) error {
		if len(nodes) == 0 {
			return fmt.Errorf("ent: Assign called without calling ScanValues")
		}
		node := nodes[len(nodes)-1]
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(columns, values)
	}
	if err := sqlgraph.QueryNodes(ctx, psaq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}

	if query := psaq.withPoll; query != nil {
		ids := make([]int, 0, len(nodes))
		nodeids := make(map[int][]*PollStudentAnswer)
		for i := range nodes {
			if nodes[i].poll_student_answers == nil {
				continue
			}
			fk := *nodes[i].poll_student_answers
			if _, ok := nodeids[fk]; !ok {
				ids = append(ids, fk)
			}
			nodeids[fk] = append(nodeids[fk], nodes[i])
		}
		query.Where(poll.IDIn(ids...))
		neighbors, err := query.All(ctx)
		if err != nil {
			return nil, err
		}
		for _, n := range neighbors {
			nodes, ok := nodeids[n.ID]
			if !ok {
				return nil, fmt.Errorf(`unexpected foreign-key "poll_student_answers" returned %v`, n.ID)
			}
			for i := range nodes {
				nodes[i].Edges.Poll = n
			}
		}
	}

	if query := psaq.withStudent; query != nil {
		ids := make([]int, 0, len(nodes))
		nodeids := make(map[int][]*PollStudentAnswer)
		for i := range nodes {
			if nodes[i].student_answers == nil {
				continue
			}
			fk := *nodes[i].student_answers
			if _, ok := nodeids[fk]; !ok {
				ids = append(ids, fk)
			}
			nodeids[fk] = append(nodeids[fk], nodes[i])
		}
		query.Where(student.IDIn(ids...))
		neighbors, err := query.All(ctx)
		if err != nil {
			return nil, err
		}
		for _, n := range neighbors {
			nodes, ok := nodeids[n.ID]
			if !ok {
				return nil, fmt.Errorf(`unexpected foreign-key "student_answers" returned %v`, n.ID)
			}
			for i := range nodes {
				nodes[i].Edges.Student = n
			}
		}
	}

	if query := psaq.withAnswer; query != nil {
		ids := make([]int, 0, len(nodes))
		nodeids := make(map[int][]*PollStudentAnswer)
		for i := range nodes {
			if nodes[i].poll_answer_student_answers == nil {
				continue
			}
			fk := *nodes[i].poll_answer_student_answers
			if _, ok := nodeids[fk]; !ok {
				ids = append(ids, fk)
			}
			nodeids[fk] = append(nodeids[fk], nodes[i])
		}
		query.Where(pollanswer.IDIn(ids...))
		neighbors, err := query.All(ctx)
		if err != nil {
			return nil, err
		}
		for _, n := range neighbors {
			nodes, ok := nodeids[n.ID]
			if !ok {
				return nil, fmt.Errorf(`unexpected foreign-key "poll_answer_student_answers" returned %v`, n.ID)
			}
			for i := range nodes {
				nodes[i].Edges.Answer = n
			}
		}
	}

	return nodes, nil
}

func (psaq *PollStudentAnswerQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := psaq.querySpec()
	return sqlgraph.CountNodes(ctx, psaq.driver, _spec)
}

func (psaq *PollStudentAnswerQuery) sqlExist(ctx context.Context) (bool, error) {
	n, err := psaq.sqlCount(ctx)
	if err != nil {
		return false, fmt.Errorf("ent: check existence: %w", err)
	}
	return n > 0, nil
}

func (psaq *PollStudentAnswerQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   pollstudentanswer.Table,
			Columns: pollstudentanswer.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: pollstudentanswer.FieldID,
			},
		},
		From:   psaq.sql,
		Unique: true,
	}
	if unique := psaq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := psaq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, pollstudentanswer.FieldID)
		for i := range fields {
			if fields[i] != pollstudentanswer.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := psaq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := psaq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := psaq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := psaq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (psaq *PollStudentAnswerQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(psaq.driver.Dialect())
	t1 := builder.Table(pollstudentanswer.Table)
	selector := builder.Select(t1.Columns(pollstudentanswer.Columns...)...).From(t1)
	if psaq.sql != nil {
		selector = psaq.sql
		selector.Select(selector.Columns(pollstudentanswer.Columns...)...)
	}
	for _, p := range psaq.predicates {
		p(selector)
	}
	for _, p := range psaq.order {
		p(selector)
	}
	if offset := psaq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := psaq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// PollStudentAnswerGroupBy is the group-by builder for PollStudentAnswer entities.
type PollStudentAnswerGroupBy struct {
	config
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (psagb *PollStudentAnswerGroupBy) Aggregate(fns ...AggregateFunc) *PollStudentAnswerGroupBy {
	psagb.fns = append(psagb.fns, fns...)
	return psagb
}

// Scan applies the group-by query and scans the result into the given value.
func (psagb *PollStudentAnswerGroupBy) Scan(ctx context.Context, v interface{}) error {
	query, err := psagb.path(ctx)
	if err != nil {
		return err
	}
	psagb.sql = query
	return psagb.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) ScanX(ctx context.Context, v interface{}) {
	if err := psagb.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from group-by.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) Strings(ctx context.Context) ([]string, error) {
	if len(psagb.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerGroupBy.Strings is not achievable when grouping more than 1 field")
	}
	var v []string
	if err := psagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) StringsX(ctx context.Context) []string {
	v, err := psagb.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = psagb.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerGroupBy.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) StringX(ctx context.Context) string {
	v, err := psagb.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from group-by.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) Ints(ctx context.Context) ([]int, error) {
	if len(psagb.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerGroupBy.Ints is not achievable when grouping more than 1 field")
	}
	var v []int
	if err := psagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) IntsX(ctx context.Context) []int {
	v, err := psagb.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = psagb.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerGroupBy.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) IntX(ctx context.Context) int {
	v, err := psagb.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from group-by.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) Float64s(ctx context.Context) ([]float64, error) {
	if len(psagb.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerGroupBy.Float64s is not achievable when grouping more than 1 field")
	}
	var v []float64
	if err := psagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) Float64sX(ctx context.Context) []float64 {
	v, err := psagb.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = psagb.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerGroupBy.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) Float64X(ctx context.Context) float64 {
	v, err := psagb.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from group-by.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) Bools(ctx context.Context) ([]bool, error) {
	if len(psagb.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerGroupBy.Bools is not achievable when grouping more than 1 field")
	}
	var v []bool
	if err := psagb.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) BoolsX(ctx context.Context) []bool {
	v, err := psagb.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a group-by query.
// It is only allowed when executing a group-by query with one field.
func (psagb *PollStudentAnswerGroupBy) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = psagb.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerGroupBy.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (psagb *PollStudentAnswerGroupBy) BoolX(ctx context.Context) bool {
	v, err := psagb.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (psagb *PollStudentAnswerGroupBy) sqlScan(ctx context.Context, v interface{}) error {
	for _, f := range psagb.fields {
		if !pollstudentanswer.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := psagb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := psagb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (psagb *PollStudentAnswerGroupBy) sqlQuery() *sql.Selector {
	selector := psagb.sql
	columns := make([]string, 0, len(psagb.fields)+len(psagb.fns))
	columns = append(columns, psagb.fields...)
	for _, fn := range psagb.fns {
		columns = append(columns, fn(selector))
	}
	return selector.Select(columns...).GroupBy(psagb.fields...)
}

// PollStudentAnswerSelect is the builder for selecting fields of PollStudentAnswer entities.
type PollStudentAnswerSelect struct {
	*PollStudentAnswerQuery
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Scan applies the selector query and scans the result into the given value.
func (psas *PollStudentAnswerSelect) Scan(ctx context.Context, v interface{}) error {
	if err := psas.prepareQuery(ctx); err != nil {
		return err
	}
	psas.sql = psas.PollStudentAnswerQuery.sqlQuery(ctx)
	return psas.sqlScan(ctx, v)
}

// ScanX is like Scan, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) ScanX(ctx context.Context, v interface{}) {
	if err := psas.Scan(ctx, v); err != nil {
		panic(err)
	}
}

// Strings returns list of strings from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) Strings(ctx context.Context) ([]string, error) {
	if len(psas.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerSelect.Strings is not achievable when selecting more than 1 field")
	}
	var v []string
	if err := psas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// StringsX is like Strings, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) StringsX(ctx context.Context) []string {
	v, err := psas.Strings(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// String returns a single string from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) String(ctx context.Context) (_ string, err error) {
	var v []string
	if v, err = psas.Strings(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerSelect.Strings returned %d results when one was expected", len(v))
	}
	return
}

// StringX is like String, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) StringX(ctx context.Context) string {
	v, err := psas.String(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Ints returns list of ints from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) Ints(ctx context.Context) ([]int, error) {
	if len(psas.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerSelect.Ints is not achievable when selecting more than 1 field")
	}
	var v []int
	if err := psas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// IntsX is like Ints, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) IntsX(ctx context.Context) []int {
	v, err := psas.Ints(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Int returns a single int from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) Int(ctx context.Context) (_ int, err error) {
	var v []int
	if v, err = psas.Ints(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerSelect.Ints returned %d results when one was expected", len(v))
	}
	return
}

// IntX is like Int, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) IntX(ctx context.Context) int {
	v, err := psas.Int(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64s returns list of float64s from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) Float64s(ctx context.Context) ([]float64, error) {
	if len(psas.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerSelect.Float64s is not achievable when selecting more than 1 field")
	}
	var v []float64
	if err := psas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// Float64sX is like Float64s, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) Float64sX(ctx context.Context) []float64 {
	v, err := psas.Float64s(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Float64 returns a single float64 from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) Float64(ctx context.Context) (_ float64, err error) {
	var v []float64
	if v, err = psas.Float64s(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerSelect.Float64s returned %d results when one was expected", len(v))
	}
	return
}

// Float64X is like Float64, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) Float64X(ctx context.Context) float64 {
	v, err := psas.Float64(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bools returns list of bools from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) Bools(ctx context.Context) ([]bool, error) {
	if len(psas.fields) > 1 {
		return nil, errors.New("ent: PollStudentAnswerSelect.Bools is not achievable when selecting more than 1 field")
	}
	var v []bool
	if err := psas.Scan(ctx, &v); err != nil {
		return nil, err
	}
	return v, nil
}

// BoolsX is like Bools, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) BoolsX(ctx context.Context) []bool {
	v, err := psas.Bools(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Bool returns a single bool from a selector. It is only allowed when selecting one field.
func (psas *PollStudentAnswerSelect) Bool(ctx context.Context) (_ bool, err error) {
	var v []bool
	if v, err = psas.Bools(ctx); err != nil {
		return
	}
	switch len(v) {
	case 1:
		return v[0], nil
	case 0:
		err = &NotFoundError{pollstudentanswer.Label}
	default:
		err = fmt.Errorf("ent: PollStudentAnswerSelect.Bools returned %d results when one was expected", len(v))
	}
	return
}

// BoolX is like Bool, but panics if an error occurs.
func (psas *PollStudentAnswerSelect) BoolX(ctx context.Context) bool {
	v, err := psas.Bool(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

func (psas *PollStudentAnswerSelect) sqlScan(ctx context.Context, v interface{}) error {
	rows := &sql.Rows{}
	query, args := psas.sqlQuery().Query()
	if err := psas.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (psas *PollStudentAnswerSelect) sqlQuery() sql.Querier {
	selector := psas.sql
	selector.Select(selector.Columns(psas.fields...)...)
	return selector
}
