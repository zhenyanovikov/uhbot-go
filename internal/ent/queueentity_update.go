// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"
	"uhbot/internal/ent/predicate"
	"uhbot/internal/ent/queue"
	"uhbot/internal/ent/queueentity"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// QueueEntityUpdate is the builder for updating QueueEntity entities.
type QueueEntityUpdate struct {
	config
	hooks    []Hook
	mutation *QueueEntityMutation
}

// Where adds a new predicate for the QueueEntityUpdate builder.
func (qeu *QueueEntityUpdate) Where(ps ...predicate.QueueEntity) *QueueEntityUpdate {
	qeu.mutation.predicates = append(qeu.mutation.predicates, ps...)
	return qeu
}

// SetName sets the "name" field.
func (qeu *QueueEntityUpdate) SetName(s string) *QueueEntityUpdate {
	qeu.mutation.SetName(s)
	return qeu
}

// SetPosition sets the "position" field.
func (qeu *QueueEntityUpdate) SetPosition(i int) *QueueEntityUpdate {
	qeu.mutation.ResetPosition()
	qeu.mutation.SetPosition(i)
	return qeu
}

// AddPosition adds i to the "position" field.
func (qeu *QueueEntityUpdate) AddPosition(i int) *QueueEntityUpdate {
	qeu.mutation.AddPosition(i)
	return qeu
}

// AddQueueIDs adds the "queue" edge to the Queue entity by IDs.
func (qeu *QueueEntityUpdate) AddQueueIDs(ids ...int) *QueueEntityUpdate {
	qeu.mutation.AddQueueIDs(ids...)
	return qeu
}

// AddQueue adds the "queue" edges to the Queue entity.
func (qeu *QueueEntityUpdate) AddQueue(q ...*Queue) *QueueEntityUpdate {
	ids := make([]int, len(q))
	for i := range q {
		ids[i] = q[i].ID
	}
	return qeu.AddQueueIDs(ids...)
}

// Mutation returns the QueueEntityMutation object of the builder.
func (qeu *QueueEntityUpdate) Mutation() *QueueEntityMutation {
	return qeu.mutation
}

// ClearQueue clears all "queue" edges to the Queue entity.
func (qeu *QueueEntityUpdate) ClearQueue() *QueueEntityUpdate {
	qeu.mutation.ClearQueue()
	return qeu
}

// RemoveQueueIDs removes the "queue" edge to Queue entities by IDs.
func (qeu *QueueEntityUpdate) RemoveQueueIDs(ids ...int) *QueueEntityUpdate {
	qeu.mutation.RemoveQueueIDs(ids...)
	return qeu
}

// RemoveQueue removes "queue" edges to Queue entities.
func (qeu *QueueEntityUpdate) RemoveQueue(q ...*Queue) *QueueEntityUpdate {
	ids := make([]int, len(q))
	for i := range q {
		ids[i] = q[i].ID
	}
	return qeu.RemoveQueueIDs(ids...)
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (qeu *QueueEntityUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(qeu.hooks) == 0 {
		if err = qeu.check(); err != nil {
			return 0, err
		}
		affected, err = qeu.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*QueueEntityMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = qeu.check(); err != nil {
				return 0, err
			}
			qeu.mutation = mutation
			affected, err = qeu.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(qeu.hooks) - 1; i >= 0; i-- {
			mut = qeu.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, qeu.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (qeu *QueueEntityUpdate) SaveX(ctx context.Context) int {
	affected, err := qeu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (qeu *QueueEntityUpdate) Exec(ctx context.Context) error {
	_, err := qeu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (qeu *QueueEntityUpdate) ExecX(ctx context.Context) {
	if err := qeu.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (qeu *QueueEntityUpdate) check() error {
	if v, ok := qeu.mutation.Name(); ok {
		if err := queueentity.NameValidator(v); err != nil {
			return &ValidationError{Name: "name", err: fmt.Errorf("ent: validator failed for field \"name\": %w", err)}
		}
	}
	return nil
}

func (qeu *QueueEntityUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   queueentity.Table,
			Columns: queueentity.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: queueentity.FieldID,
			},
		},
	}
	if ps := qeu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := qeu.mutation.Name(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: queueentity.FieldName,
		})
	}
	if value, ok := qeu.mutation.Position(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeInt,
			Value:  value,
			Column: queueentity.FieldPosition,
		})
	}
	if value, ok := qeu.mutation.AddedPosition(); ok {
		_spec.Fields.Add = append(_spec.Fields.Add, &sqlgraph.FieldSpec{
			Type:   field.TypeInt,
			Value:  value,
			Column: queueentity.FieldPosition,
		})
	}
	if qeu.mutation.QueueCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   queueentity.QueueTable,
			Columns: queueentity.QueuePrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: queue.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := qeu.mutation.RemovedQueueIDs(); len(nodes) > 0 && !qeu.mutation.QueueCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   queueentity.QueueTable,
			Columns: queueentity.QueuePrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: queue.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := qeu.mutation.QueueIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   queueentity.QueueTable,
			Columns: queueentity.QueuePrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: queue.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, qeu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{queueentity.Label}
		} else if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return 0, err
	}
	return n, nil
}

// QueueEntityUpdateOne is the builder for updating a single QueueEntity entity.
type QueueEntityUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *QueueEntityMutation
}

// SetName sets the "name" field.
func (qeuo *QueueEntityUpdateOne) SetName(s string) *QueueEntityUpdateOne {
	qeuo.mutation.SetName(s)
	return qeuo
}

// SetPosition sets the "position" field.
func (qeuo *QueueEntityUpdateOne) SetPosition(i int) *QueueEntityUpdateOne {
	qeuo.mutation.ResetPosition()
	qeuo.mutation.SetPosition(i)
	return qeuo
}

// AddPosition adds i to the "position" field.
func (qeuo *QueueEntityUpdateOne) AddPosition(i int) *QueueEntityUpdateOne {
	qeuo.mutation.AddPosition(i)
	return qeuo
}

// AddQueueIDs adds the "queue" edge to the Queue entity by IDs.
func (qeuo *QueueEntityUpdateOne) AddQueueIDs(ids ...int) *QueueEntityUpdateOne {
	qeuo.mutation.AddQueueIDs(ids...)
	return qeuo
}

// AddQueue adds the "queue" edges to the Queue entity.
func (qeuo *QueueEntityUpdateOne) AddQueue(q ...*Queue) *QueueEntityUpdateOne {
	ids := make([]int, len(q))
	for i := range q {
		ids[i] = q[i].ID
	}
	return qeuo.AddQueueIDs(ids...)
}

// Mutation returns the QueueEntityMutation object of the builder.
func (qeuo *QueueEntityUpdateOne) Mutation() *QueueEntityMutation {
	return qeuo.mutation
}

// ClearQueue clears all "queue" edges to the Queue entity.
func (qeuo *QueueEntityUpdateOne) ClearQueue() *QueueEntityUpdateOne {
	qeuo.mutation.ClearQueue()
	return qeuo
}

// RemoveQueueIDs removes the "queue" edge to Queue entities by IDs.
func (qeuo *QueueEntityUpdateOne) RemoveQueueIDs(ids ...int) *QueueEntityUpdateOne {
	qeuo.mutation.RemoveQueueIDs(ids...)
	return qeuo
}

// RemoveQueue removes "queue" edges to Queue entities.
func (qeuo *QueueEntityUpdateOne) RemoveQueue(q ...*Queue) *QueueEntityUpdateOne {
	ids := make([]int, len(q))
	for i := range q {
		ids[i] = q[i].ID
	}
	return qeuo.RemoveQueueIDs(ids...)
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (qeuo *QueueEntityUpdateOne) Select(field string, fields ...string) *QueueEntityUpdateOne {
	qeuo.fields = append([]string{field}, fields...)
	return qeuo
}

// Save executes the query and returns the updated QueueEntity entity.
func (qeuo *QueueEntityUpdateOne) Save(ctx context.Context) (*QueueEntity, error) {
	var (
		err  error
		node *QueueEntity
	)
	if len(qeuo.hooks) == 0 {
		if err = qeuo.check(); err != nil {
			return nil, err
		}
		node, err = qeuo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*QueueEntityMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = qeuo.check(); err != nil {
				return nil, err
			}
			qeuo.mutation = mutation
			node, err = qeuo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(qeuo.hooks) - 1; i >= 0; i-- {
			mut = qeuo.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, qeuo.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (qeuo *QueueEntityUpdateOne) SaveX(ctx context.Context) *QueueEntity {
	node, err := qeuo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (qeuo *QueueEntityUpdateOne) Exec(ctx context.Context) error {
	_, err := qeuo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (qeuo *QueueEntityUpdateOne) ExecX(ctx context.Context) {
	if err := qeuo.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (qeuo *QueueEntityUpdateOne) check() error {
	if v, ok := qeuo.mutation.Name(); ok {
		if err := queueentity.NameValidator(v); err != nil {
			return &ValidationError{Name: "name", err: fmt.Errorf("ent: validator failed for field \"name\": %w", err)}
		}
	}
	return nil
}

func (qeuo *QueueEntityUpdateOne) sqlSave(ctx context.Context) (_node *QueueEntity, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   queueentity.Table,
			Columns: queueentity.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: queueentity.FieldID,
			},
		},
	}
	id, ok := qeuo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "ID", err: fmt.Errorf("missing QueueEntity.ID for update")}
	}
	_spec.Node.ID.Value = id
	if fields := qeuo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, queueentity.FieldID)
		for _, f := range fields {
			if !queueentity.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != queueentity.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := qeuo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := qeuo.mutation.Name(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: queueentity.FieldName,
		})
	}
	if value, ok := qeuo.mutation.Position(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeInt,
			Value:  value,
			Column: queueentity.FieldPosition,
		})
	}
	if value, ok := qeuo.mutation.AddedPosition(); ok {
		_spec.Fields.Add = append(_spec.Fields.Add, &sqlgraph.FieldSpec{
			Type:   field.TypeInt,
			Value:  value,
			Column: queueentity.FieldPosition,
		})
	}
	if qeuo.mutation.QueueCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   queueentity.QueueTable,
			Columns: queueentity.QueuePrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: queue.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := qeuo.mutation.RemovedQueueIDs(); len(nodes) > 0 && !qeuo.mutation.QueueCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   queueentity.QueueTable,
			Columns: queueentity.QueuePrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: queue.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := qeuo.mutation.QueueIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   queueentity.QueueTable,
			Columns: queueentity.QueuePrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: queue.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	_node = &QueueEntity{config: qeuo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, qeuo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{queueentity.Label}
		} else if cerr, ok := isSQLConstraintError(err); ok {
			err = cerr
		}
		return nil, err
	}
	return _node, nil
}
