// Code generated by entc, DO NOT EDIT.

package link

import (
	"uhbot/internal/ent/predicate"

	"entgo.io/ent/dialect/sql"
)

// ID filters vertices based on their ID field.
func ID(id int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(ids) == 0 {
			s.Where(sql.False())
			return
		}
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(ids) == 0 {
			s.Where(sql.False())
			return
		}
		v := make([]interface{}, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// Teacher applies equality check predicate on the "teacher" field. It's identical to TeacherEQ.
func Teacher(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldTeacher), v))
	})
}

// Subject applies equality check predicate on the "subject" field. It's identical to SubjectEQ.
func Subject(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSubject), v))
	})
}

// SubjectType applies equality check predicate on the "subject_type" field. It's identical to SubjectTypeEQ.
func SubjectType(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSubjectType), v))
	})
}

// Platform applies equality check predicate on the "platform" field. It's identical to PlatformEQ.
func Platform(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldPlatform), v))
	})
}

// URL applies equality check predicate on the "url" field. It's identical to URLEQ.
func URL(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldURL), v))
	})
}

// TeacherEQ applies the EQ predicate on the "teacher" field.
func TeacherEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldTeacher), v))
	})
}

// TeacherNEQ applies the NEQ predicate on the "teacher" field.
func TeacherNEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldTeacher), v))
	})
}

// TeacherIn applies the In predicate on the "teacher" field.
func TeacherIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldTeacher), v...))
	})
}

// TeacherNotIn applies the NotIn predicate on the "teacher" field.
func TeacherNotIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldTeacher), v...))
	})
}

// TeacherGT applies the GT predicate on the "teacher" field.
func TeacherGT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldTeacher), v))
	})
}

// TeacherGTE applies the GTE predicate on the "teacher" field.
func TeacherGTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldTeacher), v))
	})
}

// TeacherLT applies the LT predicate on the "teacher" field.
func TeacherLT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldTeacher), v))
	})
}

// TeacherLTE applies the LTE predicate on the "teacher" field.
func TeacherLTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldTeacher), v))
	})
}

// TeacherContains applies the Contains predicate on the "teacher" field.
func TeacherContains(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldTeacher), v))
	})
}

// TeacherHasPrefix applies the HasPrefix predicate on the "teacher" field.
func TeacherHasPrefix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldTeacher), v))
	})
}

// TeacherHasSuffix applies the HasSuffix predicate on the "teacher" field.
func TeacherHasSuffix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldTeacher), v))
	})
}

// TeacherEqualFold applies the EqualFold predicate on the "teacher" field.
func TeacherEqualFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldTeacher), v))
	})
}

// TeacherContainsFold applies the ContainsFold predicate on the "teacher" field.
func TeacherContainsFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldTeacher), v))
	})
}

// SubjectEQ applies the EQ predicate on the "subject" field.
func SubjectEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSubject), v))
	})
}

// SubjectNEQ applies the NEQ predicate on the "subject" field.
func SubjectNEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldSubject), v))
	})
}

// SubjectIn applies the In predicate on the "subject" field.
func SubjectIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldSubject), v...))
	})
}

// SubjectNotIn applies the NotIn predicate on the "subject" field.
func SubjectNotIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldSubject), v...))
	})
}

// SubjectGT applies the GT predicate on the "subject" field.
func SubjectGT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldSubject), v))
	})
}

// SubjectGTE applies the GTE predicate on the "subject" field.
func SubjectGTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldSubject), v))
	})
}

// SubjectLT applies the LT predicate on the "subject" field.
func SubjectLT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldSubject), v))
	})
}

// SubjectLTE applies the LTE predicate on the "subject" field.
func SubjectLTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldSubject), v))
	})
}

// SubjectContains applies the Contains predicate on the "subject" field.
func SubjectContains(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldSubject), v))
	})
}

// SubjectHasPrefix applies the HasPrefix predicate on the "subject" field.
func SubjectHasPrefix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldSubject), v))
	})
}

// SubjectHasSuffix applies the HasSuffix predicate on the "subject" field.
func SubjectHasSuffix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldSubject), v))
	})
}

// SubjectEqualFold applies the EqualFold predicate on the "subject" field.
func SubjectEqualFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldSubject), v))
	})
}

// SubjectContainsFold applies the ContainsFold predicate on the "subject" field.
func SubjectContainsFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldSubject), v))
	})
}

// SubjectTypeEQ applies the EQ predicate on the "subject_type" field.
func SubjectTypeEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeNEQ applies the NEQ predicate on the "subject_type" field.
func SubjectTypeNEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeIn applies the In predicate on the "subject_type" field.
func SubjectTypeIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldSubjectType), v...))
	})
}

// SubjectTypeNotIn applies the NotIn predicate on the "subject_type" field.
func SubjectTypeNotIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldSubjectType), v...))
	})
}

// SubjectTypeGT applies the GT predicate on the "subject_type" field.
func SubjectTypeGT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeGTE applies the GTE predicate on the "subject_type" field.
func SubjectTypeGTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeLT applies the LT predicate on the "subject_type" field.
func SubjectTypeLT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeLTE applies the LTE predicate on the "subject_type" field.
func SubjectTypeLTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeContains applies the Contains predicate on the "subject_type" field.
func SubjectTypeContains(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeHasPrefix applies the HasPrefix predicate on the "subject_type" field.
func SubjectTypeHasPrefix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeHasSuffix applies the HasSuffix predicate on the "subject_type" field.
func SubjectTypeHasSuffix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeEqualFold applies the EqualFold predicate on the "subject_type" field.
func SubjectTypeEqualFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldSubjectType), v))
	})
}

// SubjectTypeContainsFold applies the ContainsFold predicate on the "subject_type" field.
func SubjectTypeContainsFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldSubjectType), v))
	})
}

// PlatformEQ applies the EQ predicate on the "platform" field.
func PlatformEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldPlatform), v))
	})
}

// PlatformNEQ applies the NEQ predicate on the "platform" field.
func PlatformNEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldPlatform), v))
	})
}

// PlatformIn applies the In predicate on the "platform" field.
func PlatformIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldPlatform), v...))
	})
}

// PlatformNotIn applies the NotIn predicate on the "platform" field.
func PlatformNotIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldPlatform), v...))
	})
}

// PlatformGT applies the GT predicate on the "platform" field.
func PlatformGT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldPlatform), v))
	})
}

// PlatformGTE applies the GTE predicate on the "platform" field.
func PlatformGTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldPlatform), v))
	})
}

// PlatformLT applies the LT predicate on the "platform" field.
func PlatformLT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldPlatform), v))
	})
}

// PlatformLTE applies the LTE predicate on the "platform" field.
func PlatformLTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldPlatform), v))
	})
}

// PlatformContains applies the Contains predicate on the "platform" field.
func PlatformContains(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldPlatform), v))
	})
}

// PlatformHasPrefix applies the HasPrefix predicate on the "platform" field.
func PlatformHasPrefix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldPlatform), v))
	})
}

// PlatformHasSuffix applies the HasSuffix predicate on the "platform" field.
func PlatformHasSuffix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldPlatform), v))
	})
}

// PlatformEqualFold applies the EqualFold predicate on the "platform" field.
func PlatformEqualFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldPlatform), v))
	})
}

// PlatformContainsFold applies the ContainsFold predicate on the "platform" field.
func PlatformContainsFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldPlatform), v))
	})
}

// URLEQ applies the EQ predicate on the "url" field.
func URLEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldURL), v))
	})
}

// URLNEQ applies the NEQ predicate on the "url" field.
func URLNEQ(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldURL), v))
	})
}

// URLIn applies the In predicate on the "url" field.
func URLIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.In(s.C(FieldURL), v...))
	})
}

// URLNotIn applies the NotIn predicate on the "url" field.
func URLNotIn(vs ...string) predicate.Link {
	v := make([]interface{}, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.Link(func(s *sql.Selector) {
		// if not arguments were provided, append the FALSE constants,
		// since we can't apply "IN ()". This will make this predicate falsy.
		if len(v) == 0 {
			s.Where(sql.False())
			return
		}
		s.Where(sql.NotIn(s.C(FieldURL), v...))
	})
}

// URLGT applies the GT predicate on the "url" field.
func URLGT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldURL), v))
	})
}

// URLGTE applies the GTE predicate on the "url" field.
func URLGTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldURL), v))
	})
}

// URLLT applies the LT predicate on the "url" field.
func URLLT(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldURL), v))
	})
}

// URLLTE applies the LTE predicate on the "url" field.
func URLLTE(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldURL), v))
	})
}

// URLContains applies the Contains predicate on the "url" field.
func URLContains(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldURL), v))
	})
}

// URLHasPrefix applies the HasPrefix predicate on the "url" field.
func URLHasPrefix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldURL), v))
	})
}

// URLHasSuffix applies the HasSuffix predicate on the "url" field.
func URLHasSuffix(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldURL), v))
	})
}

// URLIsNil applies the IsNil predicate on the "url" field.
func URLIsNil() predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldURL)))
	})
}

// URLNotNil applies the NotNil predicate on the "url" field.
func URLNotNil() predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldURL)))
	})
}

// URLEqualFold applies the EqualFold predicate on the "url" field.
func URLEqualFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldURL), v))
	})
}

// URLContainsFold applies the ContainsFold predicate on the "url" field.
func URLContainsFold(v string) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldURL), v))
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.Link) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.Link) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.Link) predicate.Link {
	return predicate.Link(func(s *sql.Selector) {
		p(s.Not())
	})
}
