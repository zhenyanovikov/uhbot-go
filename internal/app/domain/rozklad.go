package domain

import (
	"errors"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

var teacherRegexp = regexp.MustCompile(`<td class="(closest_pair|current_pair)">.*?<br/?>.*?title=".*?">(.*?)</a>`)

type RozkladService struct {
	RozkladKey string
}

func NewRozklad(rozkladKey string) *RozkladService {
	return &RozkladService{RozkladKey: rozkladKey}
}

func (r RozkladService) CurrentPairTeacher() (string, error) {
	url := "http://rozklad.kpi.ua/Schedules/ViewSchedule.aspx?g=" + r.RozkladKey
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	page, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	groups := teacherRegexp.FindStringSubmatch(string(page))

	if len(groups) == 3 {
		return strings.Join(strings.Fields(groups[2])[1:], " "), nil
	} else {
		return "", errors.New("regex error")
	}
}
