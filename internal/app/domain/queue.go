package domain

import (
	"context"
	"uhbot/internal/ent"
	"uhbot/internal/ent/queue"
	"uhbot/internal/ent/queueentity"
	"uhbot/internal/ent/subject"
	"uhbot/internal/ent/team"
)

type QueueService struct {
	ent *ent.Client
}

func NewQueue(ent *ent.Client) *QueueService {
	return &QueueService{ent: ent}
}

func (q QueueService) ById(queueId int) (*ent.Queue, error) {
	return q.ent.Queue.Query().
		Where(queue.ID(queueId)).
		WithAllEntities(func(q *ent.QueueEntityQuery) {
			q.Order(ent.Asc(queueentity.FieldPosition))
		}).
		Only(context.Background())
}

func (q QueueService) TeamsBySubject(subjectName string) ([]*ent.Team, error) {
	return q.ent.Team.Query().
		Where(team.HasSubjectWith(subject.Name(subjectName))).
		All(context.Background())
}

func (q QueueService) Last() (*ent.Queue, error) {
	return q.ent.Queue.Query().
		Order(ent.Desc(queue.FieldID)).
		WithAllEntities(func(q *ent.QueueEntityQuery) {
			q.Order(ent.Asc(queueentity.FieldPosition))
		}).
		First(context.Background())
}

func (q QueueService) ByTelegramMessageID(tgId int) (*ent.Queue, error) {
	return q.ent.Queue.Query().
		Where(queue.TelegramMessageID(tgId)).
		WithAllEntities(func(q *ent.QueueEntityQuery) {
			q.Order(ent.Asc(queueentity.FieldPosition))
		}).
		Only(context.Background())
}
func (q QueueService) DeleteEntity(entityId int) error {
	return q.ent.QueueEntity.
		DeleteOneID(entityId).
		Exec(context.Background())
}

func (q QueueService) Create(objective string, tgMessageId int, entities []*ent.QueueEntity) (*ent.Queue, error) {
	pollEntities := make([]*ent.QueueEntityCreate, len(entities))
	for i, entity := range entities {
		pollEntities[i] = q.ent.QueueEntity.
			Create().
			SetName(entity.Name).
			SetPosition(i + 1)
	}
	entities2, err := q.ent.QueueEntity.CreateBulk(pollEntities...).Save(context.Background())
	if err != nil {
		return nil, err
	}

	return q.ent.Queue.Create().
		AddAllEntities(entities2...).
		SetObjective(objective).
		SetTelegramMessageID(tgMessageId).
		Save(context.Background())
}
