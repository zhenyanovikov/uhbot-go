package app

import (
	"context"
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
	"html"
	"math/rand"
	"strconv"
	"strings"
	"uhbot/internal/ent"
)

func (b Bot) queue() func(m *tb.Message) {
	return func(m *tb.Message) {
		// /queue help
		// /queue students <reason>
		// /queue teams <n> <reason>

		payloadFields := strings.Fields(m.Payload)
		if len(payloadFields) == 0 {
			payloadFields = []string{"help"}
		}
		switch payloadFields[0] {
		case "help":
			_, err := b.tg.Send(m.Chat, html.EscapeString(
				"USAGE:\n"+
					"/que help\n"+
					"/que edit <delete/swap> [args...]\n"+
					"/que students <reason>\n"+
					"/que teams <subject>"))
			if err != nil {
				fmt.Println(err)
			}
		case "students":
			if len(payloadFields) == 1 {
				b.tg.Send(m.Chat, "Необходимо указать название очереди")
				return
			}

			allStudents, err := b.Students.All()
			if err != nil {
				b.Error(m, err)
				return
			}

			queueEntities := make([]*ent.QueueEntity, len(allStudents))

			for i, j := range rand.Perm(len(allStudents)) {
				queueEntities[i] = &ent.QueueEntity{Name: allStudents[j].Name}
			}

			objective := strings.Join(payloadFields[1:], " ")

			resp := queueText(&ent.Queue{
				Objective: objective,
				Edges:     ent.QueueEdges{AllEntities: queueEntities},
			})

			msg, err := b.tg.Send(m.Chat, resp)
			if err != nil {
				fmt.Println(err)
				return
			}

			entities := make([]*ent.QueueEntity, len(queueEntities))

			for i, student := range queueEntities {
				entities[i] = &ent.QueueEntity{Name: student.Name}
			}

			_, err = b.Queue.Create(objective, msg.ID, entities)
			if err != nil {
				fmt.Println(err)
				return
			}
		case "teams":
			if len(payloadFields) < 2 {
				b.tg.Send(m.Chat, html.EscapeString("USAGE:\n/que teams <subject>"))
				return
			}
			teams, err := b.Queue.TeamsBySubject(payloadFields[1])
			if err != nil {
				b.tg.Send(m.Chat, err)
				return
			}

			queueEntities := make([]*ent.QueueEntity, len(teams))

			for i, j := range rand.Perm(len(teams)) {
				queueEntities[i] = &ent.QueueEntity{Name: teams[j].Title}
			}

			resp := queueText(&ent.Queue{
				Objective: payloadFields[1],
				Edges:     ent.QueueEdges{AllEntities: queueEntities},
			})

			msg, err := b.tg.Send(m.Chat, resp)
			if err != nil {
				fmt.Println(err)
				return
			}

			_, err = b.Queue.Create(payloadFields[1], msg.ID, queueEntities)
			if err != nil {
				fmt.Println(err)
				return
			}
		case "edit":
			if len(payloadFields) < 2 {
				b.tg.Send(m.Chat, html.EscapeString("USAGE:\n/que edit <delete/swap> [args...]"))
				return
			}

			var que *ent.Queue
			var err error

			if !m.IsReply() || m.ReplyTo.Sender.ID != b.tg.Me.ID {
				que, err = b.Queue.Last()
				if err != nil {
					b.tg.Send(m.Chat, "Очередей еще не было!")
					return
				}
			} else {
				que, err = b.Queue.ByTelegramMessageID(m.ReplyTo.ID)
				if err != nil {
					b.tg.Send(m.Chat, "Это не очередь!")
					return
				}
			}

			switch payloadFields[1] {
			case "delete":
				if len(payloadFields) != 3 {
					b.tg.Send(m.Chat, html.EscapeString("USAGE:\n/que edit delete <num>"))
					return
				}
				entityId, err := strconv.Atoi(payloadFields[2])
				if err != nil {
					b.tg.Send(m.Chat, html.EscapeString("USAGE:\n/que edit delete <num>"))
					return
				}
				entityId -= 1

				if entityId < 0 || entityId >= len(que.Edges.AllEntities) {
					b.tg.Send(m.Chat, html.EscapeString("Указан неверный номер"))
					return
				}

				err = b.Queue.DeleteEntity(que.Edges.AllEntities[entityId].ID)
				if err != nil {
					b.Error(m, err)
					return
				}
				err = b.updateQueueMessage(que.TelegramMessageID, m.Chat.ID, que.ID)
				if err != nil {
					b.Error(m, err)
					return
				}
			case "swap":
				if len(payloadFields) != 4 {
					b.tg.Send(m.Chat, html.EscapeString("USAGE:\n/que edit swap <src> <dest>"))
					return
				}

				que, err := b.Queue.ById(que.ID)
				if err != nil {
					b.Error(m, err)
					return
				}

				numA, err := strconv.Atoi(payloadFields[2])
				if err != nil {
					b.tg.Send(m.Chat, html.EscapeString("Указан неверный номер"))
					return
				}
				numB, err := strconv.Atoi(payloadFields[3])
				if err != nil {
					b.tg.Send(m.Chat, html.EscapeString("Указан неверный номер"))
					return
				}

				numA -= 1
				numB -= 1

				if numA < 0 || numA >= len(que.Edges.AllEntities) {
					b.tg.Send(m.Chat, html.EscapeString("Указан неверный номер"))
					return
				}

				if numB < 0 || numB >= len(que.Edges.AllEntities) {
					b.tg.Send(m.Chat, html.EscapeString("Указан неверный номер"))
					return
				}

				entityA := que.Edges.AllEntities[numA]
				entityB := que.Edges.AllEntities[numB]

				entityB.Position = entityA.Position

				err = entityA.Update().SetPosition(numB).Exec(context.Background())
				if err != nil {
					b.tg.Send(m.Chat, err.Error())
					return
				}
				err = entityB.Update().SetPosition(numA).Exec(context.Background())
				if err != nil {
					b.tg.Send(m.Chat, err.Error())
					return
				}

				err = b.updateQueueMessage(que.TelegramMessageID, m.Chat.ID, que.ID)
				if err != nil {
					b.tg.Send(m.Chat, err.Error())
					return
				}

			}
		}
	}
}

func (b Bot) updateQueueMessage(messageId int, chatId int64, queueId int) error {
	updatedQueue, err := b.Queue.ById(queueId)
	if err != nil {
		return err
	}

	text := queueText(updatedQueue)
	b.tg.Edit(tb.StoredMessage{
		MessageID: strconv.Itoa(messageId),
		ChatID:    chatId,
	}, text)
	return err
}

func queueText(queue *ent.Queue) string {
	resp := fmt.Sprintf("Очередь на \"%s\":\n\n", html.EscapeString(queue.Objective))

	for i, entity := range queue.Edges.AllEntities {
		resp += fmt.Sprintf("%d) %s\n", i+1, entity.Name)
	}
	return resp
}
