package app

import (
	"fmt"
	"github.com/mb-14/gomarkov"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
	"strings"
	"time"
	"uhbot/internal/app/domain"
	"uhbot/internal/app/service"
	"uhbot/internal/app/textgen"
)

type Bot struct {
	tg *tb.Bot

	Rozklad  service.Rozklad
	Links    service.Links
	Polls    service.Polls
	Students service.Students
	Queue    service.Queue
}

func NewBot(token string, rozklad service.Rozklad, links service.Links, polls *domain.PollsService, students *domain.StudentsService, queue *domain.QueueService) (*Bot, error) {
	bot, err := tb.NewBot(tb.Settings{
		Token:     token,
		ParseMode: tb.ModeHTML,
		Poller:    new(tb.LongPoller),
	})
	if err != nil {
		return nil, err
	}

	return &Bot{
		tg:       bot,
		Rozklad:  rozklad,
		Links:    links,
		Polls:    polls,
		Students: students,
		Queue:    queue,
	}, nil
}

var chain *gomarkov.Chain

const defaultModel = "model"

func (b Bot) Start() {
	b.tg.Handle("харош", func(message *tb.Message) {
		if message.ReplyTo != nil && message.ReplyTo.Sender.ID == b.tg.Me.ID {
			b.tg.Forward(tb.ChatID(-1001411527964), message.ReplyTo)
		}
	})
	b.tg.Handle("/me", b.me())
	b.tg.Handle("/schedule", b.schedule())
	b.tg.Handle("/link", b.pairLink())
	b.tg.Handle("/zoom", b.pairLink())
	b.tg.Handle("/queue", b.queue())
	b.tg.Handle("/que", b.queue())
	b.tg.Handle("/poll", b.poll())
	b.tg.Handle("/reload_models", func(m *tb.Message) {
		textgen.LoadModelsFrom("./models/")
		b.tg.Send(m.Chat, strings.Join(textgen.ModelsList(), "\n"))

	})
	b.tg.Handle("/generate", func(m *tb.Message) {
		if m.Chat.ID == int64(m.Sender.ID) {
			text := textgen.Generate(defaultModel)
			b.tg.Send(m.Chat, text)
		}
	})
	lastUsages := make(map[int64]time.Time)
	timeout := time.Second * 30
	b.tg.Handle("/generate_custom", func(m *tb.Message) {
		t, ok := lastUsages[m.Chat.ID]
		if !ok {
			lastUsages[m.Chat.ID] = time.Now().Add(-timeout)
		}

		if m.Chat.ID != int64(m.Sender.ID) && !t.Before(time.Now().Add(-timeout)) {
			botMsg, _ := b.tg.Send(m.Chat, "Жди минуту, Ася запретила спамить!")
			<-time.After(5 * time.Second)
			b.tg.Delete(m)
			b.tg.Delete(botMsg)
			return
		}

		if !textgen.IsModelLoaded(m.Payload) {
			_, err := b.tg.Send(m.Chat, "У меня нет такой модели. Можно использовать следущие названия:\n"+
				strings.Join(textgen.ModelsList(), "\n"))
			if err != nil {
				fmt.Println(err)
			}
			return
		}
		text := textgen.Generate(m.Payload)
		lastUsages[m.Chat.ID] = time.Now()
		b.tg.Send(m.Chat, text)
	})
	b.tg.Handle(tb.OnCallback, func(q *tb.Callback) {
		q.Data = strings.TrimSpace(q.Data)
		if strings.HasPrefix(q.Data, "poll") {
			b.pollCallback(q)
		}
	})

	go func() {
		sendTextFunc := func() {
			text := textgen.Generate(defaultModel)
			b.tg.Send(tb.ChatID(-1001483422301), text)
		}

		c := cron.New()
		c.AddFunc("30 19 * * *", sendTextFunc)
		c.AddFunc("00 17 * * *", sendTextFunc)
		c.AddFunc("00 13 * * *", sendTextFunc)
		c.Start()
	}()

	fmt.Println("Bot started")

	b.tg.Start()
}

func generateText() string {
	tokens := []string{gomarkov.StartToken}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := chain.Generate(tokens[len(tokens)-1:])
		tokens = append(tokens, next)
	}
	text := strings.TrimSpace(strings.Join(tokens[1:len(tokens)-1], " "))
	if text == "" {
		return generateText()
	}
	return text
}

func (b Bot) Error(m *tb.Message, err error) {
	b.tg.Send(m.Chat, "Произошла ошибка")
	logrus.Error(err)
}
