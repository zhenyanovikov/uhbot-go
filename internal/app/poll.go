package app

import (
	"fmt"
	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
	"regexp"
	"strconv"
	"strings"
	"uhbot/internal/ent"
)

func (b Bot) poll() func(m *tb.Message) {
	return func(m *tb.Message) {
		stud, err := b.Students.ByTelegramId(m.Sender.ID)
		if err != nil {
			b.tg.Send(m.Chat, "Произошла ошибка")
			logrus.Error(err)
			return
		}

		if !stud.IsAdmin {
			b.tg.Send(m.Chat, "Ты не админ, пока!")
			return
		}

		if m.Payload == "" {
			lastPoll, err := b.Polls.Last()
			if err != nil {
				if ent.IsNotFound(err) {
					b.tg.Send(m.Chat, "Опросов еще не было")
					return
				}
				b.Error(m, err)
				return
			}

			pollResult(b, m, lastPoll)
			return
		}

		payloadFields := strings.Fields(m.Payload)

		switch payloadFields[0] {
		case "help":
			b.helpCommand(m)
		case "list":
			b.listCommand(m)
		case "get":
			b.getCommand(m, payloadFields)
		case "missing":
			b.missingCommand(m, payloadFields)
		case "create":
			b.createCommand(m)
		default:

		}
	}
}

func (b Bot) helpCommand(m *tb.Message) {
	b.tg.Send(m.Chat, "/poll [(helpCommand/listCommand/get/missing/create)] [args...]")
}

func (b Bot) listCommand(m *tb.Message) {
	allPolls, err := b.Polls.All()
	if err != nil {
		b.Error(m, err)
	}

	resp := "Все опросы:\n\n"

	for _, p := range allPolls {
		resp += fmt.Sprintf("(%d) %s\n", p.ID, p.Question)
	}

	b.tg.Send(m.Chat, resp)
}

func (b Bot) getCommand(m *tb.Message, payloadFields []string) {
	if len(payloadFields) != 2 {
		b.tg.Send(m.Chat, "/poll get <id>")
	}

	pollId, err := strconv.Atoi(payloadFields[1])
	if err != nil {
		b.tg.Send(m.Chat, "/poll get <id>")
	}

	pollById, err := b.Polls.ById(pollId)
	if err != nil {
		b.Error(m, err)
	}

	pollResult(b, m, pollById)
}

func pollResult(b Bot, m *tb.Message, poll *ent.Poll) {
	sb := strings.Builder{}

	sb.WriteString("Ответы на вопрос \"")
	sb.WriteString(poll.Question)
	sb.WriteString("\":\n\n")

	for _, answer := range poll.Edges.StudentAnswers {
		sb.WriteString(fmt.Sprintf(
			"<i>%s</i> - %-5s\n",
			answer.Edges.Student.Name,
			answer.Edges.Answer.Text,
		))
	}

	b.tg.Send(m.Chat, sb.String())
}

func (b Bot) missingCommand(m *tb.Message, payloadFields []string) {
	var poll *ent.Poll
	if len(payloadFields) == 1 {
		p, err := b.Polls.Last()
		if err != nil {
			b.Error(m, err)
			return
		}
		poll = p
	} else {
		id, err := strconv.Atoi(payloadFields[1])
		if err != nil {
			b.tg.Send(m.Chat, "/poll missing [id]")
			return
		}

		p, err := b.Polls.ById(id)
		if err != nil {
			b.Error(m, err)
			return
		}
		poll = p
	}

	students, err := b.Students.All()
	if err != nil {
		b.Error(m, err)
		return
	}

	for i, student := range students {
		for _, answer := range student.Edges.Answers {
			for _, studentAnswer := range poll.Edges.StudentAnswers {
				if answer.ID == studentAnswer.ID {
					students[i] = nil
				}
			}

		}
	}

	sb := strings.Builder{}

	sb.WriteString("Студенты, которые не ответили на вопрос \"")
	sb.WriteString(poll.Question)
	sb.WriteString("\":\n\n")

	for _, student := range students {
		if student != nil {
			sb.WriteString(fmt.Sprintf(
				"<i>%s</i>\n",
				student.Name,
			))
		}
	}

	b.tg.Send(m.Chat, sb.String())
}

var reg = regexp.MustCompile(`^create \((.+)\) \[(.+)] {(.+)}$`)

func (b Bot) createCommand(m *tb.Message) bool {
	submatch := reg.FindStringSubmatch(m.Payload)

	if len(submatch) != 4 {
		b.tg.Send(m.Chat, `Шаблон: /poll create (Вопрос) [Вариант 1;Вариант 2] {Ответ 1;Ответ 2}`)
		return true
	}

	answers := strings.Split(submatch[2], ";")
	response := strings.Split(submatch[3], ";")

	newPoll, err := b.Polls.Create(submatch[1], answers, response)
	if err != nil {
		b.Error(m, err)
		return true
	}

	students, err := b.Students.All()

	rows := make([]tb.Row, len(newPoll.Edges.PossibleAnswers))

	kb := &tb.ReplyMarkup{}
	for i, answer := range newPoll.Edges.PossibleAnswers {
		fmt.Println(i, answer)
		rows[i] = kb.Row(kb.Data(answer.Text, fmt.Sprintf("poll_%d_%d", newPoll.ID, i)))
	}
	kb.Inline(rows...)

	for _, stud := range students {
		_, err := b.tg.Send(tb.ChatID(stud.TelegramID), newPoll.Question, kb)
		fmt.Println(err)
	}
	return false
}
