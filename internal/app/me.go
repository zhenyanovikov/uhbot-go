package app

import (
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
)

func (b Bot) me() func(m *tb.Message) {
	return func(m *tb.Message) {
		s, err := b.Students.ByTelegramId(m.Sender.ID)
		if err != nil {
			b.tg.Send(m.Chat, "Я тебя не знаю :(")
			return
		}
		adminText := ""
		if s.IsAdmin {
			adminText = "А, ну и ты админ! Крутой!"
		}
		b.tg.Send(m.Chat, fmt.Sprintf("Ты - <b>%s</b>. \nТвой id - <b>%d</b>\n\n%s", s.Name, s.TelegramID, adminText))
	}
}
