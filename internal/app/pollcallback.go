package app

import (
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
	"strconv"
	"strings"
)

func (b Bot) pollCallback(c *tb.Callback) {
	fields := strings.Split(c.Data, "_")
	if len(fields) != 3 {
		return
	}

	pollId, err := strconv.Atoi(fields[1])
	if err != nil {
		return
	}
	answerId, err := strconv.Atoi(fields[2])
	if err != nil {
		return
	}

	_, err = b.Polls.ById(pollId)
	if err != nil {
		return
	}

	user, err := b.Students.ByTelegramId(c.Sender.ID)
	if err != nil {
		fmt.Println(err)
		return
	}

	answers, err := b.Polls.AllAnswersOnPoll(pollId)
	if err != nil {
		fmt.Println(err)
		return
	}

	_, err = b.Polls.SetAnswersOnPoll(pollId, user.ID, answers[answerId].ID)

	if err != nil {
		fmt.Println(err)
		return
	}

	b.tg.Edit(c.Message, answers[answerId].Response)
}
