package app

import (
	"fmt"
	tb "gopkg.in/tucnak/telebot.v2"
)

func (b Bot) pairLink() func(m *tb.Message) {
	return func(m *tb.Message) {
		teacher := m.Payload
		if m.Payload == "" {
			tempMsp, _ := b.tg.Send(m.Chat, "Достаю имя с Rozklad, может занять до randInt8() минут")
			t, err := b.Rozklad.CurrentPairTeacher()
			if err != nil {
				b.tg.Send(m.Chat, "Rozklad, походу, опять лежит(")
				return
			}
			b.tg.Delete(tempMsp)
			teacher = t
		}

		links, err := b.Links.ByTeacher(teacher)
		if err != nil {
			b.Error(m, err)
			return
		}

		text := ""

		for _, link := range links {
			text += fmt.Sprintf("<b>%s</b> (%s) [%s] - <a href=\"%s\">%s</a>\n\n",
				link.Subject, link.Teacher, link.SubjectType,
				*link.URL, link.Platform)
		}

		if text == "" {
			_, err = b.tg.Send(m.Chat, "Не нашел ссылки для "+teacher)
			return
		}

		b.tg.Send(m.Chat, text, &tb.SendOptions{DisableWebPagePreview: true})
	}
}
