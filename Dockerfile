FROM golang:1.17.2-alpine

RUN apk update && apk upgrade && \
    apk add --no-cache bash g++
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o main cmd/bot/main.go

CMD ["./main", "1360050495:AAE08P7lzt4Qmjysc3Qs6M47bkdeoRxtyyQ"]
